# /etc/profile.d/proxy.sh - set system wide proxy
export http_proxy=http://10.0.16.5:8888
export https_proxy=http://10.0.16.5:8888
export no_proxy="localhost, 127.0.0.0, 127.0.1.1, 127.0.1.1, .tn.esss.lu.se"
