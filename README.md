CentOS Repo and Proxy setup for TN
====

This is the temporary solution while ICS HW&I group uses the local e3 installatoin on the community CentOS installation.


## CentOS repository
Remove all existent repository configuration from /etc/yum.repos.d, and copy the ESS specific repository configurations provided in "repo" directory.  
$ sudo rm -r /etc/yum.repos.d/*.repo  
$ sudo cp repo/\*.repo /etc/yum.repos.d/  


## Proxy setup
A shell script is provided "proxy.sh" containing definitions of the required environment variables for the proxy setup.
This script exports the ip:port combination for http and https requests to go through the proxy provided by the ICS infrastructure group.
Exclusions are made for the local host and for hosts within the ESS Technical Network domain.
To deploy this configuration:  
$ sudo cp scripts/proxy.sh /etc/profile.d/proxy.sh

The details of the white-listed sites can be found here:
https://csentry.esss.lu.se/network/groups/view/tinyproxy
Under "tinyproxy_urls:", you can find the white-listeed sites.
